This repository contains the scripts for solving the problem sets on the website ROSALIND(http://rosalind.info/).

For my current progress, see [http://rosalind.info/users/ycling/].

All scripts are written with Jupyter-notebook (through Jupyter-lab), and the scripts are named and filed with the similar scheme as their corresponding problems on the website.

Among the problems that I have solved so far, the following ones are mentioned since they provide useful basic concepts:

* [Protein Translation](http://rosalind.info/problems/ptra/)
    (Similar to [Translating RNA into Protein](http://rosalind.info/problems/prot/))

    Solution: [rosalind_ptra](bioinformatics_armory/rosalind_ptra.ipynb), [rosalind_prot](bioinformatics_stronghold/rosalind_prot.ipynb)
    
    Correspondence between RNA and protein, and the importance of choosing the suitable coding scheme (codon table).

* [Finding Genes with ORFs](http://rosalind.info/problems/orfr/)
    (Similar to [Open Reading Frames](http://rosalind.info/problems/orf/))
    
    Solution: [rosalind_orfr](bioinformatics_armory/rosalind_orfr.ipynb), [rosalind_orf](bioinformatics_stronghold/rosalind_orf.ipynb)

    Multiple proteins implied by a single DNA sequence. Although the problems are stated with DNA, similar concepts could be applied to RNA as well.

* [Finding a Protein Motif](http://rosalind.info/problems/mprt/)

    Solution: [rosalind_mprt](bioinformatics_stronghold/rosalind_mprt.ipynb)

    Locating a protein motif using regular expression.


* Problem series: [Perfect Matchings and RNA Secondary Structures](http://rosalind.info/problems/pmch/), [Catalan Numbers and RNA Secondary Structures](http://rosalind.info/problems/cat/), [Motzkin Numbers and RNA Secondary Structures](http://rosalind.info/problems/motz/), [Wobble Bonding and RNA Secondary Structures](http://rosalind.info/problems/rnas/)

    Solution: [rosalind_pmch](bioinformatics_stronghold/rosalind_pmch.ipynb), [rosalind_cat](bioinformatics_stronghold/rosalind_cat.ipynb), [rosalind_motz](bioinformatics_stronghold/rosalind_motz.ipynb), [rosalind_rnas](bioinformatics_stronghold/rosalind_rnas.ipynb)

    Diversity of combinations of structures of RNA sequence.
